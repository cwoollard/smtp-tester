import smtplib, ssl

smtp_server = "email-smtp.eu-west-1.amazonaws.com"  # Set email server addres
port = 587  # For starttls
sender_email = ""  # Set from email address
receiver_email = ""  # Set destination email address
login = ""  # Set email server login account
password = ""  # Set email account password
message = """Subject: Hi there

This message is sent from Python.

To test sending email via AWS SES service"""


# Create a secure SSL context
context = ssl.create_default_context()

# Try to log in to server and send email
server = smtplib.SMTP(smtp_server,port)

try:
    server.ehlo() # Can be omitted
    server.set_debuglevel(2)
    server.starttls(context=context) # Secure the connection
    server.ehlo() # Can be omitted
    server.login(login, password)
    server.sendmail(sender_email, receiver_email, message)
except Exception as e:
    # Print any error messages to stdout
    print(e)
finally:
    server.quit()
